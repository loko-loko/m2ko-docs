import os
from jinja2 import Template


def _generate_index(docs_path: str, category: str):
    indexes = []
    for root, dirs, files in os.walk(os.path.join(docs_path, category)):
        sorted_files = sorted(files)
        f_root = root.replace(docs_path, "")
        f_root_wt_cat = f_root.replace(f"{category}/", "")
        indent_count = len(f_root_wt_cat.split("/")) - 1
        indent_suffix = "  " * indent_count + "- "

        if f_root_wt_cat != category:
            indexes.append("%(suffix)s**%(name)s**" % dict(
                suffix=indent_suffix,
                name=f_root_wt_cat
            ))
        
        for f in sorted_files:
            f_path = os.path.join(f_root, f).replace(".md", "")
            f_path_wt_cat = os.path.join(f_root_wt_cat, f).replace(".md", "")
            indent_suffix = "  " * (indent_count + 1) + "- "
            indexes.append("%(suffix)s[%(name)s](%(href)s)" % dict(
                suffix=indent_suffix,
                name=f_path_wt_cat,
                href=os.path.join("/docs/", f_path)
            ))
    return indexes


def build_category_indexes(docs_path: str, category: str, template_path: str):
    # Get index data
    indexes = _generate_index(
        docs_path=docs_path,
        category=category
    )
    index_tpl_file = os.path.join(
        template_path,
        "index.md.j2"
    )
    # Read template
    with open(index_tpl_file, "r") as f:
        template = Template(f.read())
    # Build
    output = template.render(
        category=category,
        indexes=indexes
    )
    # Write templating file
    index_output_file = os.path.join(docs_path, category, "index.md")
    with open(index_output_file, "w") as f:
        f.write(output)


def _get_categories(docs_path: str):
    _, dirs, _ = list(os.walk(docs_path))[0]
    return dirs


def template_config(base_path: str, template_path: str, categories: list):
    docrus_files = [
        "docusaurus.config.js",
        "sidebars.js"
    ]

    for docrus_file in docrus_files:
        # Get template + config file
        tpl_file = os.path.join(
            template_path,
            f"{docrus_file}.j2"
        )
        cfg_file = os.path.join(
            base_path,
            docrus_file
        )
        # Read template
        with open(tpl_file, "r") as f:
            template = Template(f.read())
        # Build
        output = template.render(categories=categories)
        # Write templating file
        with open(cfg_file, "w") as f:
            f.write(output)

def build_categories(docs_path: str):
    # Get categories names
    category_names = _get_categories(
        docs_path=docs_path
    )
    categories = []
    for category_name in category_names:
        category_index = os.path.join(
            category_name,
            "index"
        )
        categories.append({
            "name": category_name,
            "index": category_index
        }) 
    return categories

def main():

    BASE_PATH = os.path.dirname(os.path.abspath(__file__))
    DOCS_PATH = os.path.join(BASE_PATH, "docs/")
    TEMPLATE_PATH = os.path.join(BASE_PATH, "templates/")

    categories = build_categories(
        docs_path=DOCS_PATH
    )

    for category in categories:
        build_category_indexes(
            docs_path=DOCS_PATH,
            category=category["name"],
            template_path=TEMPLATE_PATH
        )

    template_config(
        base_path=BASE_PATH,
        template_path=TEMPLATE_PATH,
        categories=categories
    )

if __name__ == "__main__":
    main()

