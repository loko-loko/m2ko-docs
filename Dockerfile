FROM node:14-alpine

WORKDIR /app

RUN apk add \
      curl \
      python3 \
      py3-pip \
      rsync \
    && pip install -U pip \
    && pip install jinja2

COPY . /app

RUN mv ./entrypoint.sh / \
    && chmod 755 /entrypoint.sh \
    && npm i docusaurus-lunr-search --save \
    && npm run swizzle docusaurus-lunr-search SearchBar -- --danger \
    && yarn \
    && yarn build

ENTRYPOINT ["/entrypoint.sh"]

