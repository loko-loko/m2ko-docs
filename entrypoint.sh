#!/bin/sh

DOCS_ARCHIVE="m2ko-markdown-docs-master"
DOCS_ARCHIVE_URL="https://gitlab.com/loko-loko/m2ko-markdown-docs/-/archive/master/${DOCS_ARCHIVE}.tar.gz"

# Download docs
echo "INFO: Download docs from: ${DOCS_ARCHIVE_URL}"
curl -LO $DOCS_ARCHIVE_URL

# Uncompress
echo "INFO: Uncompress docs archive"
tar xvzf "${DOCS_ARCHIVE}.tar.gz"

# Move content to docs folder
echo "INFO: Sync docs to (/app/docs)"
rsync -av --delete ${DOCS_ARCHIVE}/docs/ /app/docs/

echo "INFO: Sync images (/app/.images)"
[[ ! -e /app/.images ]] && mkdir -p /app/.images
rsync -av --delete ${DOCS_ARCHIVE}/.images/ /app/.images/

echo "INFO: Build new sidecar"
python3 /app/setup-docs.py

# Clean archive
echo "INFO: Clean archive: ${DOCS_ARCHIVE}"
rm -rf "${DOCS_ARCHIVE}*"

# Build new docs
echo "INFO: Build website"
yarn build

# Serve website
echo "INFO: Run website"
yarn run serve

